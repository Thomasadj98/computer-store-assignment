// bank
const bankBalanceElement = document.getElementById("bank_balance");
const loanBtnElement = document.getElementById("loan_btn");
const loanBalanceElement = document.getElementById("loanbalance");
const loanTitleElement = document.getElementById("loantitle");

// work 
const workBalanceElement = document.getElementById("work_balance");
const bankBtnElement = document.getElementById("bank_btn");
const workBtnElement = document.getElementById("work_btn");
const payLoanBtnElement = document.getElementById("payloanbtn");

// product select
const computersElement = document.getElementById("computers");
const featuresElement = document.getElementById("features");

// product details
const priceElement = document.getElementById("price");
const payElement = document.getElementById("pay");
const computerTitleElement = document.getElementById("computertitle");
const computerDescriptionElement = document.getElementById("detaildescription");
const detailImageElement = document.getElementById("detailimage");


// Fetch API data

let computers = [];

fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToSupply(computers));


const addComputersToSupply = (computers) => {
    computers.forEach(x => addComputerToSupply(x));
    priceElement.innerText = "€ " + computers[0].price;

    featuresElement.innerHTML = "";
    computers[0].specs.forEach(function (feature) {
        var featureElement = document.createElement("li");
        featureElement.value = feature;
        featureElement.appendChild(document.createTextNode(feature));
        featuresElement.appendChild(featureElement);
    });
    computerTitleElement.innerText = computers[0].title;
    computerDescriptionElement.innerText = computers[0].description;
    detailImageElement.setAttribute('src', computers[0].image);
    detailImageElement.setAttribute('alt', computers[0].title);
}

const addComputerToSupply = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement);
}


// Add product information 

const handleComputerSupplyChange = event => {
    const selectedComputer = computers[event.target.selectedIndex];
    priceElement.innerText = "€ " + selectedComputer.price;

    featuresElement.innerHTML = "";
    selectedComputer.specs.forEach(function (feature) {
        var featureElement = document.createElement("li");
        featureElement.value = feature;
        featureElement.appendChild(document.createTextNode(feature));
        featuresElement.appendChild(featureElement);
    });
    computerTitleElement.innerText = selectedComputer.title;
    computerDescriptionElement.innerText = selectedComputer.description;
    detailImageElement.setAttribute('src', selectedComputer.image);
    detailImageElement.setAttribute('alt', selectedComputer.title);
}

computersElement.addEventListener("change", handleComputerSupplyChange);


// Pay for Computer

var makePurchase = function () {
    var selectedComputer = computers[computersElement.selectedIndex];
    var computerPrice = selectedComputer.price;
    if (bankBalanceStatus < computerPrice) {
        alert("Your balance is too low at the moment. Please come back richer.");
        return;
    }
    bankBalanceStatus -= computerPrice;
    updateBalances();
    alert("Congratulations on your purchase.");
};

payElement.addEventListener("click", makePurchase);


var updateBalances = function () {
    workBalanceElement.innerText = "€ " + workBalanceStatus;
    bankBalanceElement.innerText = "€ " + bankBalanceStatus;
    loanBalanceElement.innerText = "€ " + loanBalanceStatus;
};


// WORK SECTION

let workBalanceStatus = 0.0;

const setWorkBalance = () => {
    workBalanceStatus = workBalanceStatus + 100;
    workBalanceElement.innerText = "€ " + workBalanceStatus;
}

workBtnElement.addEventListener("click", setWorkBalance);

// Bank btn

let bankBalanceStatus = 0.0;

const setBankBalance = () => {
    if (loanBalanceStatus <= 0) {
        bankBalanceStatus = bankBalanceStatus + workBalanceStatus;
        bankBalanceElement.innerText = "€ " + bankBalanceStatus;
        workBalanceStatus = 0.0;
        workBalanceElement.innerText = workBalanceStatus;
    } else {
        loanBalanceStatus -= (workBalanceStatus * 0.1);
        bankBalanceStatus += (workBalanceStatus * 0.9);
        bankBalanceElement.innerText = "€ " + bankBalanceStatus;
        workBalanceStatus = 0.0;
        workBalanceElement.innerText = workBalanceStatus;
    }
    updateBalances();
}

bankBtnElement.addEventListener("click", setBankBalance);


// LOAN SECTION

let loanBalanceStatus = 0.0;

var getLoan = function () {
    if (loanBalanceStatus > 0) {
        alert("You already have a loan, please pay off the outstanding loan in order to get a new one.");
        return;
    }
    var amount = parseInt(window.prompt("How much do you need?", ""));
    if (amount === 0 || isNaN(amount) || amount > bankBalanceStatus * 2) {
        alert("You need to have more capital in order to take on a bigger loan.");
        return;
    }
    console.log(amount);
    loanBalanceStatus = loanBalanceStatus + amount;
    bankBalanceStatus += amount;
    updateBalances();
    showLoanElements(true);
};


var showLoanElements = function (show) {
    payLoanBtnElement.style.visibility = show ? "visible" : "hidden";
    loanTitleElement.style.visibility = show ? "visible" : "hidden";
    loanBalanceElement.style.visibility = show ? "visible" : "hidden";
    // loanBtnElement.disabled = show ? true : false;
};

loanBtnElement.addEventListener("click", getLoan);


var payoffLoan = function () {
    if (loanBalanceStatus === 0 || workBalanceStatus === 0)
        return;
    if (loanBalanceStatus < workBalanceStatus) {
        workBalanceStatus -= loanBalanceStatus;
        loanBalanceStatus = 0.0;
        showLoanElements(false);
    }
    else {
        loanBalanceStatus -= workBalanceStatus;
        workBalanceStatus = 0.0;
    }
    updateBalances();
};

payLoanBtnElement.addEventListener("click", payoffLoan);